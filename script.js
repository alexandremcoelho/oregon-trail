function Dog (name, breed, isGoodBoy) {
    this.name = name
    this.breed = breed
    this.isGoodBoy = isGoodBoy
 }
 
 Dog.prototype = {
    constructor: Dog,
    sit: function () {
        // sitting code here
    },
    fetch: function () {
        // fetching code here
    }
 }

function Traveler (name) {
    this.name = name
    this.food = 1
    this.isHealthy = true
}

Traveler.prototype = {
    constructor: Traveler,
    hunt: function () {
        this.food += 2
    },
    eat: function () {
        if (this.food > 0){
            this.food--
        }else{
            this.isHealthy = false
        }
    }
}

function Wagon (capacity, passengers = []) {
    this.capacity = capacity
    this.passengers = passengers
}

Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function () {
        return this.capacity - this.passengers.length
    },
    join: function (traveler) {
        if (this.getAvailableSeatCount() > 0){
            this.passengers.push(traveler)
        }
    },
    shouldQuarantine: function () {
        let output = false
        for (let i=0; i < this.passengers.length; i++){
            if (this.passengers[i].isHealthy === false) {
                output = true
            }
        }
        return output
    },
    totalFood: function () {
        let allFood = 0
        for (let i=0; i < this.passengers.length; i++){
        allFood += this.passengers[i].food
        }
        return allFood
    }
}

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);